package com.example.suoheng002.gistext;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ZoomControls;

import com.esri.core.geometry.Envelope;
import com.esri.core.internal.util.d;

import java.io.Serializable;

import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;

import java.util.ArrayList;
import java.util.List;

import com.esri.android.map.GraphicsLayer;
import com.esri.android.map.LocationDisplayManager;
import com.esri.android.map.MapView;
import com.esri.android.map.ags.ArcGISTiledMapServiceLayer;
import com.esri.android.map.event.OnStatusChangedListener;
import com.esri.core.geometry.Geometry;
import com.esri.core.geometry.GeometryEngine;
import com.esri.core.geometry.LinearUnit;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.Polygon;
import com.esri.core.geometry.Polyline;
import com.esri.core.geometry.SpatialReference;
import com.esri.core.geometry.Unit;
import com.esri.core.map.Graphic;
import com.esri.core.symbol.PictureMarkerSymbol;
import com.esri.core.symbol.SimpleFillSymbol;
import com.esri.core.symbol.SimpleLineSymbol;
import com.esri.core.symbol.SimpleMarkerSymbol;


public class MainActivity extends AppCompatActivity {

    Button GPS_btn;
    MapView mMapView;
    ArcGISTiledMapServiceLayer tileLayer;
    Point point;
    Point wgspoint, currentPoint;
    GraphicsLayer gLayerPos;
    PictureMarkerSymbol locationSymbol;
    LocationManager locMag;
    Location loc;
    Point mapPoint;
    TextView txtview;
    ZoomControls zoomCtrl;


    LocationDisplayManager lDisplayManager = null;
    GraphicsLayer gpsGraphicsLayer, gpsGraphicsLayer1;
    Polyline mPolyline;
    int pointCount = 0;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mMapView = (MapView) findViewById(R.id.map);
        tileLayer = new ArcGISTiledMapServiceLayer(
                "http://cache1.arcgisonline.cn/ArcGIS/rest/services/ChinaOnlineCommunity/MapServer");
        mMapView.addLayer(tileLayer);

        gpsGraphicsLayer = new GraphicsLayer();
        mMapView.addLayer(gpsGraphicsLayer);


        gpsGraphicsLayer1 = new GraphicsLayer();
        mMapView.addLayer(gpsGraphicsLayer1);


        mPolyline = new Polyline();

        mMapView.setOnStatusChangedListener(new OnStatusChangedListener() {
            @Override
            public void onStatusChanged(Object source, STATUS status) {
                if (source == mMapView && status == STATUS.INITIALIZED) {
                    lDisplayManager = mMapView.getLocationDisplayManager();//获取LocationDisplayManager
                    lDisplayManager.setAutoPanMode(LocationDisplayManager.AutoPanMode.OFF);
                    lDisplayManager.setShowLocation(false);//不显示当前位置，坐标系不一致坐标偏移严重
                    lDisplayManager.setShowPings(false);
                    lDisplayManager.setAccuracyCircleOn(false);
                    lDisplayManager.setAllowNetworkLocation(true);
                    lDisplayManager.setLocationListener(new LocationListener() {

                        @Override
                        public void onLocationChanged(Location loc) {
                            Toast.makeText(MainActivity.this, "111111111111111111111onLocationChanged", Toast.LENGTH_SHORT).show();

                            aa(loc);
//                            addGPSEngine();

                        }

                        @Override
                        public void onProviderDisabled(String arg0) {
                            Toast.makeText(MainActivity.this, "2222222222222222222222onProviderDisabled", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onProviderEnabled(String arg0) {
                            Toast.makeText(MainActivity.this, "333333333333333333333333onProviderEnabled", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onStatusChanged(String arg0, int arg1,
                                                    Bundle arg2) {
                            Toast.makeText(MainActivity.this, "444444444444444444onStatusChanged", Toast.LENGTH_SHORT).show();

                        }
                    });

                    lDisplayManager.start();
                }
            }


        });


        zoomCtrl = (ZoomControls) findViewById(R.id.zoomCtrl);
        zoomCtrl.setOnZoomInClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mMapView.zoomin();
            }
        });
        zoomCtrl.setOnZoomOutClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                mMapView.zoomout();
            }
        });
        gLayerPos = new GraphicsLayer();
        mMapView.addLayer(gLayerPos);
        locationSymbol = new PictureMarkerSymbol(this.getResources().getDrawable(R.drawable.aaa));


        locMag = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);


        //获取所有可用的位置提供器
        GPS_btn = (Button) findViewById(R.id.GPS_btn);
        GPS_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // TODO Auto-generated method stub
                final List<String> providers = locMag.getProviders(true);
                for (String provider : providers) {
                    if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MainActivity.this,
                            Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        return;
                    }
                    loc = locMag.getLastKnownLocation(provider);
                    LocationListener locationListener = new LocationListener() {
                        public void onLocationChanged(Location location) {


                        }

                        public void onProviderDisabled(String arg0) {
                        }

                        public void onProviderEnabled(String arg0) {
                        }

                        public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
                        }
                    };
                    locMag.requestLocationUpdates(provider, 2000, 10, locationListener);
                    if (loc != null) {
                        markLocation1(loc);

                    }
                }
            }
        });


    }

    private void aa(Location loc) {
        try {

            gpsGraphicsLayer.removeAll();
            double locx = loc.getLongitude();//经度
            double locy = loc.getLatitude();//纬度


            wgspoint = new Point(locx, locy);
            //定位到所在位置
            mapPoint = (Point) GeometryEngine.project(wgspoint, SpatialReference.create(4326), mMapView.getSpatialReference());
            SimpleMarkerSymbol ptSym = new SimpleMarkerSymbol(Color.BLUE, 15,
                    SimpleMarkerSymbol.STYLE.CIRCLE);

            Graphic graphic = new Graphic(mapPoint, ptSym, null);
            if (pointCount == 0) {
                Toast.makeText(MainActivity.this, "进入pointCount == 0", Toast.LENGTH_SHORT).show();

                mPolyline.startPath(mapPoint.getX(), mapPoint.getY());//方法为空？ com.esri.core.geometry.GeometryException:
                // This operation should not be performed on an empty geometry.

                mMapView.zoomTo(mapPoint, 17);
            } else {
                Toast.makeText(MainActivity.this, "进入pointCount !== 0", Toast.LENGTH_SHORT).show();
                mPolyline.lineTo(mapPoint.getX(),

                        mapPoint.getY());//点画线

                mMapView.centerAt(mapPoint, true);

            }
            gpsGraphicsLayer.removeAll();
            SimpleLineSymbol lineSym = new SimpleLineSymbol(Color.RED, 10);

            Graphic g = new Graphic(mPolyline, lineSym);
            //画线
            gpsGraphicsLayer.addGraphic(g);

            pointCount++;

            //定位
            gpsGraphicsLayer.addGraphic(graphic);

            Toast.makeText(MainActivity.this, "涂鸦信息qian" +
                    "=="+Util.CalDistance(mapPoint.getX(), mapPoint.getY(), xx, yy) , Toast.LENGTH_SHORT).show();
            if (Util.CalDistance(mapPoint.getX(), mapPoint.getY(), xx, yy) > 50) {

                Toast.makeText(MainActivity.this, "涂鸦信息：进入=="+Util.CalDistance(mapPoint.getX(), mapPoint.getY(), xx, yy) , Toast.LENGTH_SHORT).show();
                addGPSEngine1(mapPoint.getX(),mapPoint.getY());
                addGPSEngine();
                xx = mapPoint.getX();
                yy = mapPoint.getY();
            }

        } catch (Exception e) {
            Toast.makeText(MainActivity.this, "画线  错误信息" + e.getMessage(), Toast.LENGTH_SHORT).show();
            System.out.println(e.getMessage());
        }
    }
    double xx = 0;
    double yy = 0;

    /**
     * 涂鸦1
     * @param x
     * @param y
     */
    private void addGPSEngine1(double x, double y) {
        try {


            Polyline  pp = new Polyline();
            gpsGraphicsLayer1.removeAll();
            wgspoint = new Point(x, y);
            //定位到所在位置
            mapPoint = (Point) GeometryEngine.project(wgspoint, SpatialReference.create(4326), mMapView.getSpatialReference());
            if (pointCount == 0) {
                pp.startPath(mapPoint.getX(), mapPoint.getY());//方法为空？ com.esri.core.geometry.GeometryException:
            } else {
                pp.lineTo(mapPoint.getX(),
                        mapPoint.getY());//点画线
            }
            gpsGraphicsLayer1.removeAll();
            SimpleLineSymbol lineSym = new SimpleLineSymbol(Color.GREEN, 110);
            lineSym.setAlpha(25);
            Graphic g = new Graphic(pp, lineSym);
            //画线
            gpsGraphicsLayer1.addGraphic(g);
        } catch (Exception e) {
            Toast.makeText(MainActivity.this, "涂鸦  错误信息" + e.getMessage(), Toast.LENGTH_SHORT).show();
            System.out.println(e.getMessage());
        }
    }
    /**
     * 涂鸦1
     */
    private void bb(Location loc) {
        try {

            gpsGraphicsLayer1.removeAll();
            double locx = loc.getLongitude();//经度
            double locy = loc.getLatitude();//纬度


            wgspoint = new Point(locx, locy);
            //定位到所在位置
            mapPoint = (Point) GeometryEngine.project(wgspoint, SpatialReference.create(4326), mMapView.getSpatialReference());
            SimpleMarkerSymbol ptSym = new SimpleMarkerSymbol(Color.BLUE, 15,
                    SimpleMarkerSymbol.STYLE.CIRCLE);


            Graphic graphic = new Graphic(mapPoint, ptSym, null);
            if (pointCount == 0) {

                mPolyline.startPath(mapPoint.getX(), mapPoint.getY());//方法为空？ com.esri.core.geometry.GeometryException:
                // This operation should not be performed on an empty geometry.

//                mMapView.zoomTo(mapPoint, 17);
            } else {

                mPolyline.lineTo(mapPoint.getX(),

                        mapPoint.getY());//点画线
//                mMapView.centerAt(mapPoint, true);
            }
            gpsGraphicsLayer1.removeAll();

            SimpleLineSymbol sfs = new SimpleLineSymbol(Color.RED, 110);
            sfs.setAlpha(25);

            Graphic g = new Graphic(mPolyline, sfs);

            gpsGraphicsLayer1.addGraphic(g);


            //定位
//            gpsGraphicsLayer1.addGraphic(graphic);
            //涂鸦


//                                addGPSEngine(locx, locy);
        } catch (Exception e) {
            Toast.makeText(MainActivity.this, "  错误信息  " + e.getMessage(), Toast.LENGTH_SHORT).show();
            System.out.println(e.getMessage());
        }
    }

    private final double metersToDegrees(double distanceInMeters) {
        return distanceInMeters / 111319.5;
    }

    boolean enableSketching = true;
    Geometry firstGeometry = null;
    double bufferDist = 3000;
//    SpatialReference spatialRef = SpatialReference.create(102100);

    Unit unit;
    Polygon pg;
    Geometry geom[] = null;
    ArrayList<Geometry> arrayList = new ArrayList<>();
    int i;
    SpatialReference spatialRef = SpatialReference.create(102100);

    /**
     * 添加轨迹涂鸦
     */
    private void addGPSEngine() {

        Toast.makeText(MainActivity.this, "777777777777777777777777addGPSEngine", Toast.LENGTH_SHORT).show();


        gpsGraphicsLayer1.removeAll();
        if (firstGeometry != null) {
            try {

                unit = spatialRef.getUnit();

                if (unit.getUnitType() == Unit.UnitType.ANGULAR) {

                } else {

                    unit = Unit.create(LinearUnit.Code.METER);
                }

                for (i = 0; arrayList.size() > i; i++) {

                    pg = GeometryEngine.buffer(arrayList.get(i), spatialRef, 15, unit); //null表示采用地图单位

//                    GeometryEngine.distance()
                    SimpleFillSymbol sfs = new SimpleFillSymbol(Color.GREEN);

                    sfs.setOutline(new SimpleLineSymbol(Color.GREEN, 0,
                            SimpleLineSymbol.STYLE.SOLID));
                    sfs.setAlpha(15);

                    Graphic g = new Graphic(pg, sfs);
                    sfs.setAlpha(15);
                    gpsGraphicsLayer1.addGraphic(g);
                }


            } catch (Exception ex) {

                Toast.makeText(MainActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();

            }
            enableSketching = false;

        }
    }

    private void markLocation1(Location location) {

        gLayerPos.removeAll();
        double locx = location.getLongitude();//经度
        double locy = location.getLatitude();//纬度
        wgspoint = new Point(locx, locy);
        //定位到所在位置
        mapPoint = (Point) GeometryEngine.project(wgspoint, SpatialReference.create(4326), mMapView.getSpatialReference());
        Graphic graphic = new Graphic(mapPoint, locationSymbol);
        gLayerPos.addGraphic(graphic);
        mMapView.centerAt(mapPoint, true);
        mMapView.setScale(100);
        mMapView.setMaxResolution(300);

    }

    private void markLocation(Location location) {


        gLayerPos.removeAll();
        double locx = location.getLongitude();
        double locy = location.getLatitude();
        wgspoint = new Point(locx, locy);
        mapPoint = (Point) GeometryEngine.project(wgspoint, SpatialReference.create(4326), mMapView.getSpatialReference());

        //图层的创建
        Graphic graphicPoint = new Graphic(mapPoint, locationSymbol);
        gLayerPos.addGraphic(graphicPoint);
        /*划线
        if (startPoint == null) {
			poly=new Polyline();
			startPoint = mapPoint;
			poly.startPath((float) startPoint.getX(),
					(float) startPoint.getY());
			Graphic graphicLine = new Graphic(startPoint,new SimpleLineSymbol(Color.RED,2));
			gLayerGps.addGraphic(graphicLine);
		}*/
        mPolyline.lineTo((float) mapPoint.getX(), (float) mapPoint.getY());
        gLayerPos.addGraphic(new Graphic(mPolyline, new SimpleLineSymbol(Color.BLACK, 2)));

    }


    /*public void onLocationChanged(Location location) {
        System.out.println("进入：onLocationChanged");

        if (null != location) {
//            Point p = (Point) GeometryEngine.project(new Point(location.getLongitude() + (double) ((new Random().nextInt(9)) * 0.001), location.getLatitude() + (double) ((new Random().nextInt(9)) * 0.001)),
//                    SpatialReference.create(SpatialReference.WKID_WGS84),
//                    mMapView.getSpatialReference());
            Point p = (Point) GeometryEngine.project(new Point(location.getLongitude(), location.getLatitude()),
                    SpatialReference.create(SpatialReference.WKID_WGS84),
                    mMapView.getSpatialReference());
            currentPoint = p;
            switch (track_status) {
                //当轨迹处于开始和继续状态时记录轨迹
                case ClassStr.start:
                case ClassStr.mcontinue:
                    if (pointCount == 0) {
                        picSymbol_startpoint = new PictureMarkerSymbol(context.getResources().getDrawable(R.drawable.start_point));
                        startpointGraphicsLayer.addGraphic(new Graphic(p, picSymbol_startpoint));
                        mPathPolyline.startPath(p.getX(), p.getY());
                        mMapView.zoomTo(p, 17);
                    } else {
                        mPathPolyline.lineTo(p.getX(), p.getY());//点画线
//                        mMapView.centerAt(p, true);
                    }

                    SimpleLineSymbol lineSym = new SimpleLineSymbol(Color.RED, 10);
                    Graphic g = new Graphic(mPathPolyline, lineSym);
                    pathGraphicsLayer.addGraphic(g);
                    pointCount++;

                    //长度
                    double l = Math.round(engine.geodesicLength(mPathPolyline, mMapView.getSpatialReference(), null));
                    mileage_tv.setText(l + "");

                    updateTrackBean();

                    break;
                //当轨迹处于暂停和停止状态时不记录轨迹
                case ClassStr.pause:
                case ClassStr.stop:
                    break;
            }

            if (null != coord) {
                coord.setVisibility(View.VISIBLE);
                coord.setText(StringFormat.Point2String(currentPoint));
            }
        }
    }*/
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mMapView.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMapView.unpause();
    }

}